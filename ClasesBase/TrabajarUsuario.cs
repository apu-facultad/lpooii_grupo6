﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using ClasesBase.dtos;

namespace ClasesBase
{
    public class TrabajarUsuario
    {
        static SqlConnection conexion = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
        /** Extrae usuario de la base de datos.
         * @param user nombre del usuario
         * @param pass contraseña del usuario
         * Nombre de columna idem al modelo
         */
        static public DataTable selectUsuario(string user, string pass)
        {
            Usuario usuario = new Usuario();
            conexion.Open();
            SqlCommand comando = new SqlCommand("loginUsuario", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@user", user);
            comando.Parameters.AddWithValue("@pass", pass);
            DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            dataAdapter.Fill(dataTable);
            conexion.Close();
            return dataTable;
        }

        static public Usuario getUsuario(string nombre, string password)
        {
            DataTable dt = TrabajarUsuario.selectUsuario(nombre, password);
            List<Usuario> usuarios = ModelSchema.CreateListFromTable<Usuario>(dt);

            if (usuarios.Count == 0)
            {
                return null;
            }
            return usuarios[0];
        }

        /** Extrae todos los usuario de la base de datos.
         * Nombre de columna idem al modelo
         */
        static public DataTable selectUsuarios()
        {
            Usuario usuario = new Usuario();
            conexion.Open();
            SqlCommand comando = new SqlCommand("listarUsuarios", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            dataAdapter.Fill(dataTable);
            conexion.Close();
            return dataTable;
        }

        public ObservableCollection<UsuarioRol> traerUsuario()
        {
            ObservableCollection<UsuarioRol> collectionUsuario = new ObservableCollection<UsuarioRol>();
            DataTable dt = TrabajarUsuario.selectUsuarios();
            List<UsuarioRol> usuarios = ModelSchema.CreateListFromTable<UsuarioRol>(dt);
            if (usuarios.Count == 0)
            {
                return null;
            }
            foreach (UsuarioRol user in usuarios)
            {
                collectionUsuario.Add(user);
            }
            return collectionUsuario;
         }

        /** Extrae usuario de la base de datos.
         * @param user nombre del usuario
         * Nombre de columna idem al modelo
         */
        static public DataTable selectUsuarioWhereName(string name)
        {
            Usuario usuario = new Usuario();
            conexion.Open();
            SqlCommand comando = new SqlCommand("buscarUsuario", conexion);
            comando.Parameters.AddWithValue("@name", name);
            comando.CommandType = CommandType.StoredProcedure;
            DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            dataAdapter.Fill(dataTable);
            conexion.Close();
            return dataTable;
        }

        static public Usuario buscarUsuarioNombre(String name)
        {
            DataTable dt = TrabajarUsuario.selectUsuarioWhereName(name);
            List<Usuario> usuarios = ModelSchema.CreateListFromTable<Usuario>(dt);
            if (usuarios.Count == 0)
            {
                return null;
            }
            return usuarios[0];
        }

        static public Usuario buscarUsuarioId(int id)
        {
            Usuario usuario = new Usuario();
            conexion.Open();
            SqlCommand comando = new SqlCommand("buscarUsuarioID", conexion);
            comando.Parameters.AddWithValue("@id", id);
            comando.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            conexion.Close();
            if (dataTable.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                usuario.Usu_ID = (int)dataTable.Rows[0]["usu_id"];
                usuario.Usu_NombreUsuario = dataTable.Rows[0]["usu_nombreUsuario"].ToString();
                usuario.Usu_Password = dataTable.Rows[0]["usu_password"].ToString();
                usuario.Usu_ApellidoNombre = dataTable.Rows[0]["usu_apellidoNombre"].ToString();
                usuario.Rol_Codigo = (int)dataTable.Rows[0]["rol_codigo"];


            }
            return usuario;
        }

        static public void AltaUsuario(Usuario usuario)
        {
            SqlCommand comando = new SqlCommand("insertarUsuario", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@name", usuario.Usu_NombreUsuario);
            comando.Parameters.AddWithValue("@pass", usuario.Usu_Password);
            comando.Parameters.AddWithValue("@nombre", usuario.Usu_ApellidoNombre);
            comando.Parameters.AddWithValue("@rol", usuario.Rol_Codigo);
            conexion.Open();
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        static public void BajaUsuario(int id)
        {
            conexion.Open();
            SqlCommand comando = new SqlCommand();
            comando.CommandText = "eliminarUsuario";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Connection = conexion;
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        public void ModificacionUsuario(Usuario usuario)
        {
            SqlCommand comando = new SqlCommand("modificarUsuario", conexion);
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id", usuario.Usu_ID);
            comando.Parameters.AddWithValue("@name", usuario.Usu_NombreUsuario);
            comando.Parameters.AddWithValue("@pass", usuario.Usu_Password);
            comando.Parameters.AddWithValue("@nombre", usuario.Usu_ApellidoNombre);
            comando.Parameters.AddWithValue("@rol", usuario.Rol_Codigo);
            conexion.Open();
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        static public Usuario validarLogin()
        {
            Usuario usuario = new Usuario();
            usuario.Usu_NombreUsuario = "";
            usuario.Usu_Password = "";
            return usuario;
        }
    }
}
