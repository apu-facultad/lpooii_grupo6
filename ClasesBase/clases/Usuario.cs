﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ClasesBase
{
    public class Usuario : INotifyPropertyChanged , IDataErrorInfo
    {

        private int usu_ID;
        private string usu_NombreUsuario;
        private string usu_Password;
        private string usu_ApellidoNombre;
        private int rol_Codigo;
        // private Rol rol;
        public event PropertyChangedEventHandler PropertyChanged;

        public Usuario(int id, string us, string pass, string name, int rol) 
        {
            this.usu_ID = id;
            this.usu_NombreUsuario = us;
            this.usu_Password = pass;
            this.usu_ApellidoNombre = name;
            this.rol_Codigo = rol;
        }

        public Usuario()
        {
            // TODO: Complete member initialization
        }

        public void Notificar(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string error = null;
                switch (columnName)
                {
                    case "Usu_NombreUsuario":
                        error = validarNombre();
                        break;
                    case "Usu_Password":
                        error = validarPassword();
                        break;
                    case "Usu_ApellidoNombre":
                        error = validarApellidoNombre();
                        break;
                    case "Rol_Codigo":
                        error = validarRol();
                        break;
                }
                return error;
            }
        }

        private string validarNombre()
        {
            if (String.IsNullOrEmpty(Usu_NombreUsuario))
            {
                return "el valor del campo es obligatorio";
            }
            return null;
        }

        private string validarPassword()
        {
            if (String.IsNullOrEmpty(Usu_Password))
            {
                return "el valor del campo es obligatorio";
            }
            return null;
        }

        private string validarApellidoNombre()
        {
            if (String.IsNullOrEmpty(Usu_ApellidoNombre))
            {
                return "el valor del campo es obligatorio";
            }
            return null;
        }

        private string validarRol()
        {
            if (String.IsNullOrEmpty(Rol_Codigo.ToString()))
            {
                return "el valor del caompo es obligatorio";
            }
            return null;
        }

        public int Usu_ID
        {
            get { return usu_ID; }
            set { usu_ID = value; }
        }
        

        public string Usu_NombreUsuario
        {
            get { return usu_NombreUsuario; }
            set { usu_NombreUsuario = value; }
        }
        

        public string Usu_Password
        {
            get { return usu_Password; }
            set { usu_Password = value; }
        }
        

        public string Usu_ApellidoNombre
        {
            get { return usu_ApellidoNombre; }
            set { usu_ApellidoNombre = value; }
        }


        public int Rol_Codigo
        {
            get { return rol_Codigo; }
            set { rol_Codigo = value; }
        }
    }
}
