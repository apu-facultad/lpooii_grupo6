﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;
using System.Data;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using System.Windows.Threading;
using System.Windows.Controls.Primitives;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para winPelicula.xaml
    /// </summary>
    public partial class winPelicula : Window
    {
        private bool mediaPlayerIsPlaying = false;
        private bool userIsDraggingSlider = false;
        public winPelicula()
        {
            InitializeComponent();
            cargarLista();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }
        
        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if ((txtTitulo.Text != "") && (txtDuracion.Text != "") && (cmbClase.Text != "") && (cmbGenero.Text != ""))
            {
                if ((imgPelicula.Source != null) && (mePlayer.Source != null))
                {
                    MessageBoxResult result = MessageBox.Show("¿Desea agregar pelicula?", "Agregar Pelicula", MessageBoxButton.YesNo);
                    if (result == MessageBoxResult.Yes)
                    {
                        int cantidadPeliculasMore = TrabajarPelicula.getPeliculasCount() + 1;
                        Pelicula pelicula = new Pelicula(cantidadPeliculasMore.ToString(), txtTitulo.Text, txtDuracion.Text, cmbClase.Text, cmbGenero.Text, imgPelicula.Source.ToString(), mePlayer.Source.ToString());
                        MessageBox.Show("Se agregó correctamente \n \n" + "titulo: " + pelicula.Peli_Titulo + "\n" + "Duración: " + pelicula.Peli_Duracion + "min \n" + "género: " + pelicula.Peli_Clase + "\n" + "clase: " + pelicula.Peli_Genero + "\n");

                        TrabajarPelicula.insertar_pelicula(pelicula);
                        /** Bug: no se recarga la lista */
                        // this.listView1.DataContext = TrabajarPelicula.traerPeliculas(); // recarga los datos con los guardados en base de datos
                        limpiar();
                        cargarLista();
                    }
                    else
                    {

                    }
                }
                else {
                    MessageBox.Show("Seleccione imagen y video");
                }
            }
            else
            {
                MessageBox.Show("Ingrese todos los campos");
            }
        }

            public void limpiar(){
                txtCodigo.Text = "";
                txtTitulo.Text = "";
                 txtDuracion.Text = "";
                 cmbClase.Text = "";
                 cmbGenero.Text = "";
                 mePlayer.Source = null;
                 imgPelicula.Source = null;
                 btnAgregar.Visibility = Visibility.Visible;
                 btnModificar.Visibility = Visibility.Hidden;
                 btnEliminar.Visibility = Visibility.Hidden;
                }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if ((txtCodigo.Text == "") && (txtDuracion.Text == "") && (txtTitulo.Text == ""))
            {
                MessageBox.Show("Complete todos los campos", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Seguro que desea MODIFICAR?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        Pelicula pelicula = new Pelicula();
                        pelicula.Peli_Codigo = txtCodigo.Text;
                        pelicula.Peli_Titulo = txtTitulo.Text;
                        pelicula.Peli_Duracion = txtDuracion.Text;
                        pelicula.Peli_Genero = cmbGenero.Text;
                        pelicula.Peli_Clase = cmbClase.Text;
                        pelicula.Peli_imagen = imgPelicula.Source.ToString();
                        pelicula.Peli_avance = mePlayer.Source.ToString();
                        TrabajarPelicula.modificarPelicula(pelicula);
                        limpiar();
                        cargarLista();
                        btnAgregar.Visibility = Visibility.Visible;
                        btnModificar.Visibility = Visibility.Hidden;
                        break;
                }
            } 
        }

        private void cargarLista()
        {
            List<Pelicula> listPelicula = new List<Pelicula>();
            DataTable dt = TrabajarPelicula.traerPeliculas();
            foreach (DataRow row in dt.Rows)
            {
                Pelicula pelicula = new Pelicula();
                pelicula.Peli_Clase = row["peli_clase"].ToString();
                pelicula.Peli_Codigo = row["peli_codigo"].ToString();
                pelicula.Peli_Duracion = row["peli_duracion"].ToString();
                pelicula.Peli_Genero = row["peli_genero"].ToString();
                pelicula.Peli_Titulo = row["peli_titulo"].ToString();
                pelicula.Peli_imagen = row["peli_imagen"].ToString();
                pelicula.Peli_avance = row["peli_avance"].ToString();
                listPelicula.Add(pelicula);
            }
            gridPelicula.ItemsSource = listPelicula;
        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void gridPelicula_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnModificar.Visibility = Visibility.Visible;
            btnEliminar.Visibility = Visibility.Visible;
            btnAgregar.Visibility = Visibility.Hidden;
            Pelicula pelicula = new Pelicula();
            pelicula = (Pelicula)this.gridPelicula.SelectedItem;
            if (pelicula != null)
            {
                txtCodigo.Text = pelicula.Peli_Codigo;
                txtTitulo.Text = pelicula.Peli_Titulo;
                txtDuracion.Text = pelicula.Peli_Duracion;
                cmbGenero.Text = pelicula.Peli_Genero;
                cmbClase.Text = pelicula.Peli_Clase;
                try {
                    imgPelicula.Source = new BitmapImage(new Uri(pelicula.Peli_imagen, UriKind.Absolute));
                }
                catch {
                    imgPelicula.Source = null;
                }
                try
                {
                    mePlayer.Source = new Uri(pelicula.Peli_avance, UriKind.Absolute);
                }
                catch {
                    mePlayer.Source = null;
                }
            }
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
            btnModificar.Visibility = Visibility.Hidden;
            btnEliminar.Visibility = Visibility.Hidden;
            btnAgregar.Visibility = Visibility.Visible;
            imgPelicula.Source = null;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Seguro que desea ELIMINAR?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.OK:
                    Pelicula peli = (Pelicula)this.gridPelicula.SelectedItem;
                    TrabajarPelicula.eliminarPelicula(peli.Peli_Codigo);
                    cargarLista();
                    limpiar();
                    MessageBox.Show("Registro ELIMINADO!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
            }
            limpiar();
        }

        private void ValidarTextBoxNumber(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btnImagen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileJpg = new OpenFileDialog();
            if (fileJpg.ShowDialog() == true)
            {
                Uri fileUri = new Uri(fileJpg.FileName);
                imgPelicula.Source = new BitmapImage(fileUri);
            }

        }


        private void btnVideo_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Media files (*.mp3;*.mpg;*.mpeg,*.mp4)|*.mp3;*.mpg;*.mpeg;*.mp4|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                mePlayer.Source = new Uri(openFileDialog.FileName);
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            if ((mePlayer.Source != null) && (mePlayer.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                sliProgress.Minimum = 0;
                sliProgress.Maximum = mePlayer.NaturalDuration.TimeSpan.TotalSeconds;
                sliProgress.Value = mePlayer.Position.TotalSeconds;
            }
        }

        private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Media files (*.mp3;*.mpg;*.mpeg)|*.mp3;*.mpg;*.mpeg|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                mePlayer.Source = new Uri(openFileDialog.FileName);
        }

        private void Play_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (mePlayer != null) && (mePlayer.Source != null);
        }

        private void Play_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mePlayer.Play();
            mediaPlayerIsPlaying = true;
        }

        private void Pause_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mediaPlayerIsPlaying;
        }

        private void Pause_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mePlayer.Pause();
        }

        private void Stop_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = mediaPlayerIsPlaying;
        }

        private void Stop_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            mePlayer.Stop();
            mediaPlayerIsPlaying = false;
        }

        private void sliProgress_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        private void sliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            mePlayer.Position = TimeSpan.FromSeconds(sliProgress.Value);
        }

        private void sliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblProgressStatus.Text = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"hh\:mm\:ss");
        }

        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            mePlayer.Volume += (e.Delta > 0) ? 0.1 : -0.1;
        }


        
    }
}
