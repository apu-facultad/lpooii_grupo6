﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{
    public class TrabajarRol
    {
        static SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
        static DataTable extraccionRol()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "listarRoles";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);
            return dt;
        }

        public static List<Rol> getRols()
        {
            List<Rol> listaRols = new List<Rol>();
            DataTable datosBD = extraccionRol();
            if (datosBD.Rows.Count == 0)
            {
                return listaRols;
            } else {
                foreach (DataRow row in datosBD.Rows)
                {
                    Rol rol = new Rol();
                    rol.Rol_Codigo = (int)row["rol_codigo"];
                    rol.Rol_Descripcion = row["rol_descripcion"].ToString();
                    listaRols.Add(rol);
                }
            }
            return listaRols;
        }
    }
}
