﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{

    public class TrabajarCliente
    {
        static SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);

        public static Cliente iniciarCliente() {

            Cliente client = new Cliente();
            return client;
        }

        public static Cliente obtenerCliente(int dni)
        {
            Cliente cliente = new Cliente();
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "buscarCliente";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@dni", dni);
            cmd.Connection = cn;

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                string var = dt.Rows[0]["cli_dni"].ToString();
                cliente.Cli_DNI = Convert.ToInt32(var); 
                cliente.Cli_Apellido = dt.Rows[0]["cli_apellido"].ToString();
                cliente.Cli_Nombre = dt.Rows[0]["cli_nombre"].ToString();
                cliente.Cli_Telefono = dt.Rows[0]["cli_telefono"].ToString();
                cliente.Cli_Email = dt.Rows[0]["cli_email"].ToString();
                return cliente;
            }
        }

        public static DataTable traerClientes()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "listarClientes";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);
            return dt;
        }

        public static void insertar_Cliente(Cliente oCliente)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "insertarCliente";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@dni", oCliente.Cli_DNI);
            cmd.Parameters.AddWithValue("@nom", oCliente.Cli_Nombre);
            cmd.Parameters.AddWithValue("@ape", oCliente.Cli_Apellido);
            cmd.Parameters.AddWithValue("@tel", oCliente.Cli_Telefono);
            cmd.Parameters.AddWithValue("@email", oCliente.Cli_Email);
           
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void modificarCliente(Cliente cli)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "modificarCliente";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@dniP", cli.Cli_DNI);
            cmd.Parameters.AddWithValue("@nombreP", cli.Cli_Nombre);
            cmd.Parameters.AddWithValue("@apellidoP", cli.Cli_Apellido);
            cmd.Parameters.AddWithValue("@telefonoP", cli.Cli_Telefono);
            cmd.Parameters.AddWithValue("@emailP", cli.Cli_Email);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void eliminarCliente(int dni)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "eliminarCliente";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@dniP", dni);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }
    }
}
