﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace ClasesBase
{
    public class Cliente : IDataErrorInfo
    {
        private int cli_DNI;
        private string cli_Nombre;
        private string cli_Apellido;
        private string cli_Telefono;
        private string cli_Email;
        // private Ticket ticket;
        
        public Cliente(){}

        public Cliente(int dni, string nombre, string apellido, string telefono, string email)
        {
            this.cli_DNI = dni;
            this.cli_Nombre = nombre;
            this.cli_Apellido = apellido;
            this.cli_Telefono = telefono;
            this.cli_Email = email;
        }

       
        public int Cli_DNI
        {
            get { return cli_DNI; }
            set { cli_DNI = value; }
        }

        public string Cli_Nombre
        {
            get { return cli_Nombre; }
            set { cli_Nombre = value; }
        }

        public string Cli_Apellido
        {
            get { return cli_Apellido; }
            set { cli_Apellido = value; }
        }

        public string Cli_Telefono
        {
            get { return cli_Telefono; }
            set { cli_Telefono = value; }
        }

        public string Cli_Email
        {
            get { return cli_Email; }
            set { cli_Email = value; }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string error = null;
                switch (columnName)
                {
                    case "Cli_DNI":
                        error = validarDNI();
                        break;
                    case "Cli_Apellido":
                        error = validarApellido();
                        break;
                    case "Cli_Nombre":
                        error = validarNombre();
                        break;
                    case "Cli_Telefono":
                        error = validarTelefono();
                        break;
                    case "Cli_Email":
                        error = validarEmail();
                        break;
                }
                return error;
            }

        }

        private string validarDNI()
        {
            if (Cli_DNI <= 0)
            {
                return "El campo DNI es obligatorio";
            }
            return null;
        }
        private string validarApellido()
        {
            if (String.IsNullOrEmpty(Cli_Apellido))
            {
                return "El campo Apellido es obligatorio";
            }
            return null;
        }
        private string validarNombre()
        {
            if (String.IsNullOrEmpty(Cli_Nombre))
            {
                return "El campo Nombre es obligatorio";
            }
            return null;
        }
        private string validarTelefono()
        {
            if (String.IsNullOrEmpty(Cli_Telefono))
            {
                return "El campo Teléfono es obligatorio";
            }
            return null;
        }
                
        private string validarEmail()
        {
            if (String.IsNullOrEmpty(Cli_Email))
            {
                return "El campo email es obligatorio";
            }
            Regex regEx;

            regEx = new Regex(@"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\." +

                @"(\w{2}|(com|net|org|edu|int|mil|gov|arpa|" +

                @"biz|aero|name|coop|info|pro|museum))$");

            if (!regEx.IsMatch(Cli_Email))
            {
                return "El campo Correo Electrónico no tiene un formato correcto";
            }
            return null;
        }
    }
}
