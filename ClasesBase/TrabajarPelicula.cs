﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{
    public class TrabajarPelicula
    {
        static SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
        public static DataTable traerPeliculas()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "listarPeliculas";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);
            return dt;
        }

        public static void insertar_pelicula(Pelicula newPelicula)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "insertarPelicula";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@peli_Clase", newPelicula.Peli_Clase);
            cmd.Parameters.AddWithValue("@peli_Codigo", newPelicula.Peli_Codigo);
            cmd.Parameters.AddWithValue("@peli_Duracion", newPelicula.Peli_Duracion);
            cmd.Parameters.AddWithValue("@peli_Genero", newPelicula.Peli_Genero);
            cmd.Parameters.AddWithValue("@peli_Titulo", newPelicula.Peli_Titulo);
            cmd.Parameters.AddWithValue("@peli_Imagen", newPelicula.Peli_imagen);
            cmd.Parameters.AddWithValue("@peli_Avance", newPelicula.Peli_avance);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void modificarPelicula(Pelicula peli)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "modificarPelicula";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@codigoP", peli.Peli_Codigo);
            cmd.Parameters.AddWithValue("@tituloP", peli.Peli_Titulo);
            cmd.Parameters.AddWithValue("@duracionP", peli.Peli_Duracion);
            cmd.Parameters.AddWithValue("@generoP", peli.Peli_Genero);
            cmd.Parameters.AddWithValue("@claseP", peli.Peli_Clase);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        static public int getPeliculasCount()
        {
            DataTable dt = traerPeliculas();
            List<Pelicula> peliculas = ModelSchema.CreateListFromTable<Pelicula>(dt);
            return peliculas.Count;
        }

        public static void eliminarPelicula(string cod)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "eliminarPelicula";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@codP", cod);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public DataTable listarPelicula()
        {
            cn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT peli_titulo AS DATOS FROM Pelicula ORDER BY peli_codigo";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);
            cn.Close();
            return dt;
        }

        public string traerCodigoPelicula(string titulo)
        {
            string aux = "";
            cn.Open();

            SqlCommand cmd = new SqlCommand("SELECT * FROM Pelicula WHERE peli_titulo = @titulo", cn);
            cmd.Parameters.AddWithValue("@titulo", titulo);

            //cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();
            if (dt.Rows.Count == 0)
            {
                return aux;
            }
            else
            {
                foreach (DataRow row in dt.Rows)
                {
                    aux = row["peli_codigo"].ToString();
                }
                return aux;
            }
        }
        
        static public DataTable selectPeliculaWhereCodigo(string pelicodigo)
        {
            cn.Open();
            SqlCommand comando = new SqlCommand("buscarPeliculaID", cn);
            comando.Parameters.AddWithValue("@pelicodigo", pelicodigo);
            comando.CommandType = CommandType.StoredProcedure;
            DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            dataAdapter.Fill(dataTable);
            cn.Close();
            return dataTable;
        }

        static public Pelicula buscarPeliculaByCodigo(String pelicodigo)
        {
            DataTable dt = selectPeliculaWhereCodigo(pelicodigo);
            List<Pelicula> listaPeliculas = ModelSchema.CreateListFromTable<Pelicula>(dt);
            if (listaPeliculas.Count == 0)
            {
                return null;
            }
            return listaPeliculas[0];
        }
    }
}
