﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;
using ClasesBase.dtos;

namespace Vistas
{
    /// <summary>
    /// Interaction logic for winUsuarioLista.xaml
    /// </summary>
    public partial class winUsuarioLista : Window
    {
        private CollectionViewSource vistaColeccionFiltrada;

        public winUsuarioLista()
        {
            InitializeComponent();
            //se accede al recurso collectionViewSource
            vistaColeccionFiltrada = Resources["VISTA_USER"] as CollectionViewSource; //x:Key del Collection
        }

        private void eventVistaUsuario_Filter(object sender, FilterEventArgs e)
        {
            UsuarioRol userRolDTO = e.Item as UsuarioRol;
            //se realiza la busqueda por usuario
            if (userRolDTO.Usu_NombreUsuario.StartsWith(textBox_User.Text, StringComparison.CurrentCultureIgnoreCase))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void textBox_User_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (vistaColeccionFiltrada != null)
            {
                //se llama al metodo eventVistaUsuario_Filter a medida q se escriba en el textBox
                vistaColeccionFiltrada.Filter += eventVistaUsuario_Filter;
            }
        }



        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnVistPrevia_Click(object sender, RoutedEventArgs e)
        {
            winVistaPreviaUsuario vista = new winVistaPreviaUsuario(vistaColeccionFiltrada);
            vista.Owner = this;
            vista.ShowDialog();
        }
    }
}
