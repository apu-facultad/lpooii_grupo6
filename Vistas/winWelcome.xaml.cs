﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para winWelcome.xaml
    /// </summary>
    public partial class winWelcome : Window
    {
        const string ADMINISTRADOR = "Administrador";
        const string VENDEDOR = "Vendedor";

        public winWelcome()
        {
            InitializeComponent();
        }

        /*EVENTOS PRINCIPALES*/
        private void btnIngreso_Click(object sender, RoutedEventArgs e)
        {
            /** control de campo vacios */
            if (Login.Usuario.Equals("") || Login.Password.Equals(""))
            {
                MessageBox.Show("Debe completar los campos");
                return;
            }

            /** control si existe usuario */
            Usuario usuarioLogueado = TrabajarUsuario.getUsuario(Login.Usuario, Login.Password);
            if (usuarioLogueado == null)
            {
                MessageBox.Show("Usuario no Registrado");
                return;
            }

            /** creando nuevo inicio de usuario */
            winMain main = new winMain();
            main.UsuarioLogeado = usuarioLogueado;
            String descripcionRol = (int)main.UsuarioLogeado.Rol_Codigo == 1 ? ADMINISTRADOR : VENDEDOR;
            MessageBox.Show("Bienvenido " + descripcionRol);
            
            // ventana de presentacion Grupo con Audio
            WinAudio miAudio = new WinAudio();
            miAudio.ShowDialog();

            main.ShowDialog();
            limpiar();

        }

        private void limpiar()
        {
            Login.Usuario = "";
            Login.Password = "";
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

    }
}
