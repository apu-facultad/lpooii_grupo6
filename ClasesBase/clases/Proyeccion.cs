﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;

namespace ClasesBase
{
    public class Proyeccion : IDataErrorInfo
    {
        public Proyeccion(int codigo, string fecha, string hora, int sala) {
            this.proy_Codigo = codigo;
            this.proy_Fecha = fecha;
            this.proy_Hora = hora;
            this.sala_Numero = sala;
        
        }

        public Proyeccion(int codigo, string fecha, string hora, int sala,string pelicula)
        {
            this.proy_Codigo = codigo;
            this.proy_Fecha = fecha;
            this.proy_Hora = hora;
            this.sala_Numero = sala;
            this.peli_codigo = pelicula;
        }

        public Proyeccion() { 
            
        }

        private int proy_Codigo;

        public int Proy_Codigo
        {
            get { return proy_Codigo; }
            set { proy_Codigo = value; }
        }
        private string proy_Fecha;

        public string Proy_Fecha
        {
            get { return proy_Fecha; }
            set { proy_Fecha = value; }
        }
        private string proy_Hora;

        public string Proy_Hora
        {
            get { return proy_Hora; }
            set { proy_Hora = value; }
        }
        private int sala_Numero;

        public int Sala_Numero
        {
            get { return sala_Numero; }
            set { sala_Numero = value; }
        }

        private string peli_codigo;

        public string Peli_Codigo
        {
            get { return peli_codigo; }
            set { peli_codigo = value; }
        }

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string mensaje = string.Empty;
                //validacion //
                if (columnName == "Proy_Codigo")
                {
                    if (Proy_Codigo <= 0 || Proy_Codigo > 999)
                    {
                        mensaje = "Ingrese valor valido para Codigo Pelicula";
                    }

                }
                if (columnName == "Sala_Numero")
                {
                    /*
                    if (Convert.ToInt32(Sala_Numero) <= 0 || Convert.ToInt32(Sala_Numero) > 3)
                    {
                        mensaje = "valor incorrecto para Sala, sugerencia: 1..3";
                    }
                    */
                }

                if (columnName == "Proy_Hora")
                {
                    if (string.IsNullOrEmpty(Proy_Hora))
                    {
                        mensaje = "Hora de proyeccion, obligatorio..";
                    }

                }

                //fin validacion//

                return mensaje;
            }
        }

    }
}
