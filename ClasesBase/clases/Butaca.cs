﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase
{
    public class Butaca
    {
        public Butaca() { }

        public Butaca(string fila, string numero, string sala) {
            this.but_Fila = fila;
            this.but_numero = numero;
            this.but_NroSala = sala;
        }

        public Butaca(int codigo, string fila, string numero, string sala, string estado)
        {
            this.but_Codigo = codigo;
            this.but_Fila = fila;
            this.but_numero = numero;
            this.but_NroSala = sala;
            this.but_Estado = estado;
        }

        private int but_Codigo;

        public int But_Codigo
        {
            get { return but_Codigo; }
            set { but_Codigo = value; }
        }

        private string but_Fila;

        public string But_Fila
        {
            get { return but_Fila; }
            set { but_Fila = value; }
        }
        private string but_numero;

        public string But_Numero
        {
            get { return but_numero; }
            set { but_numero = value; }
        }
        private string but_NroSala;

        public string But_NroSala
        {
            get { return but_NroSala; }
            set { but_NroSala = value; }
        }

        private string but_Estado;

        public string But_Estado
        {
            get { return but_Estado; }
            set { but_Estado = value; }
        }

    }
}
