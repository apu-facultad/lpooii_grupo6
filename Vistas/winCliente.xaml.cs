﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;
using System.Text.RegularExpressions;
using System.Data;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para winCliente.xaml
    /// </summary>
    public partial class winCliente : Window
    {
        public winCliente()
        {
            InitializeComponent();
            limpiar();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ObjectDataProvider odp = (ObjectDataProvider)this.Resources["dataClient"];
            cargarLista();
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if ((txtNombre.Text != "") && (txtApellido.Text != "") && (txtDNI.Text != "") && (txtTelefono.Text != "") && (txtMail.Text != ""))
            {
                MessageBoxResult result = MessageBox.Show("¿Desea agregar cliente?", "Agregar Cliente", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    Cliente oCliente = new Cliente(Convert.ToInt32(txtDNI.Text), txtNombre.Text, txtApellido.Text, txtTelefono.Text, txtMail.Text);
                    TrabajarCliente.insertar_Cliente(oCliente);

                    //MessageBox.Show("Se agregó correctamente \n \n" + "Nombre: " + cliente1.Cli_Nombre + "\n" + "Apellido: " + cliente1.Cli_Apellido + "\n" + "DNI: " + cliente1.Cli_DNI + "\n" + "Telefono: " + cliente1.Cli_Telefono + "\n" + "E-mail: " + cliente1.Cli_Email);
                    limpiar();
                    cargarLista();
                }
                else
                {
                }
               
                

            }
            else
            {
                MessageBox.Show("Ingrese todos los campos");
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            Cliente uCliente = new Cliente();
            String buscado = txtBuscar.Text;
            if (buscado != "")
            {
                int num = Convert.ToInt32(buscado);
                uCliente = TrabajarCliente.obtenerCliente(num);
                if (uCliente == null)
                {
                    MessageBox.Show("Cliente no encontrado", "Error");
                }
                else
                {
                    MessageBox.Show(uCliente.Cli_Apellido + "\n" + uCliente.Cli_Nombre + "\n" + uCliente.Cli_DNI, "Cliente Encontrado:");
                }
            }
            else {
                MessageBox.Show("Ingresar DNI a buscar");
            }
        }
        private void textBox_Dni_LostFocus(object sender, RoutedEventArgs e)
        {
            btnAgregar.Visibility = Visibility.Visible;
            txtApellido.Text = "";
            txtNombre.Text = "";
            txtMail.Text = "";
            txtTelefono.Text = "";

            Cliente cliente = new Cliente();
            if (txtDNI.Text != "")
            {
                cliente = TrabajarCliente.obtenerCliente(int.Parse(txtDNI.Text));
                if (cliente != null)
                {
                    btnModificar.Visibility = Visibility.Visible;
                    btnAgregar.Visibility = Visibility.Hidden;
                    btnEliminar.Visibility = Visibility.Visible;
                    txtDNI.IsEnabled = false;
                    txtApellido.Text = cliente.Cli_Apellido;
                    txtNombre.Text = cliente.Cli_Nombre;
                    txtMail.Text = cliente.Cli_Email;
                    txtTelefono.Text = cliente.Cli_Telefono;
                }
                else
                {
                    //button_Modificar.Visibility = Visibility.Hidden;
                }
            }

        }

        private void ValidarTextBoxNumber(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if ((txtNombre.Text == "") && (txtApellido.Text == "") && (txtDNI.Text == "") && (txtTelefono.Text == "") && (txtMail.Text == ""))
            {
                MessageBox.Show("Complete todos los campos", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Seguro que desea MODIFICAR?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        Cliente cliente = new Cliente();
                        cliente.Cli_Apellido = txtApellido.Text;
                        cliente.Cli_DNI = Convert.ToInt32(txtDNI.Text);
                        cliente.Cli_Email = txtMail.Text;
                        cliente.Cli_Nombre = txtNombre.Text;
                        cliente.Cli_Telefono = txtTelefono.Text;
                        TrabajarCliente.modificarCliente(cliente);
                        txtDNI.IsEnabled = true;
                        cargarLista();
                        limpiar();
                        break;
                }
            }    
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void cargarLista()
        {
            List<Cliente> listCliente = new List<Cliente>();
            DataTable dt = TrabajarCliente.traerClientes();
            foreach (DataRow row in dt.Rows)
            {
                Cliente cliente = new Cliente();
                cliente.Cli_DNI = int.Parse(row["cli_dni"].ToString());
                cliente.Cli_Apellido = row["cli_apellido"].ToString();
                cliente.Cli_Nombre = row["cli_nombre"].ToString();
                cliente.Cli_Telefono = row["cli_telefono"].ToString();
                cliente.Cli_Email = row["cli_email"].ToString();
                listCliente.Add(cliente);
            }
            gridClientes.ItemsSource = listCliente;
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            txtDNI.IsEnabled=true;
            btnEliminar.Visibility = Visibility.Hidden;
            btnAgregar.Visibility = Visibility.Visible;
            btnModificar.Visibility = Visibility.Hidden;
            limpiar();
        }

        public void limpiar(){
            txtDNI.IsEnabled = true;
            btnModificar.Visibility = Visibility.Hidden;
            btnEliminar.Visibility = Visibility.Hidden;
            btnAgregar.Visibility = Visibility.Visible;
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtDNI.Text = "";
            txtTelefono.Text = "";
            txtMail.Text = "";
        }

        private void gridCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnModificar.Visibility = Visibility.Visible;
            btnEliminar.Visibility = Visibility.Visible;
            btnAgregar.Visibility = Visibility.Hidden;
            Cliente cliente= new Cliente();
            cliente = (Cliente)this.gridClientes.SelectedItem;
            if (cliente != null)
            {
                txtDNI.IsEnabled = false;
                txtDNI.Text = cliente.Cli_DNI.ToString();
                txtApellido.Text = cliente.Cli_Apellido;
                txtNombre.Text = cliente.Cli_Nombre;
                txtMail.Text = cliente.Cli_Email;
                txtTelefono.Text = cliente.Cli_Telefono;
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Seguro que desea ELIMINAR?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.OK:
                    Cliente cliente = (Cliente)this.gridClientes.SelectedItem;
                    TrabajarCliente.eliminarCliente(cliente.Cli_DNI);
                    cargarLista();
                    limpiar();
                    MessageBox.Show("Registro ELIMINADO!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
            }
            limpiar();
        }
    }
}
