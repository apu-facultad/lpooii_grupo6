﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using ClasesBase;
using ClasesBase.dtos;

namespace Vistas
{
    /// <summary>
    /// Interaction logic for winUsuario.xaml
    /// </summary>
    public partial class winUsuario : Window
    {
        CollectionView Vista;
        ObservableCollection<UsuarioRol> listaUsuarios;
        List<Rol> listaRol;
        TrabajarUsuario trabajarUsuario = new TrabajarUsuario();

        public winUsuario()
        {
            InitializeComponent();
            Update_rols();

        }
        private void Update_rols()
        {
            listaRol = TrabajarRol.getRols();
                foreach (Rol rol in listaRol){
                    cmbRol.Items.Add(rol.Rol_Descripcion);
            }
        } 
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ObjectDataProvider odp = (ObjectDataProvider)this.Resources["LIST_USUARIO"];
            listaUsuarios = odp.Data as ObservableCollection<UsuarioRol>;
            Vista = (CollectionView)CollectionViewSource.GetDefaultView(content.DataContext);
        }

        private void btnAlta_Click(object sender, RoutedEventArgs e)
        {
            if (esCampoVacio(groupBox1))
            {
                MessageBox.Show("Complete todos los campos", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Seguro que desea guardar?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        
                        TrabajarUsuario.AltaUsuario(setearObjeto());
                        Usuario usr = new Usuario();
                        UsuarioRol usrAux = new UsuarioRol();
                        usr = setearObjeto();

                        MessageBox.Show("Registro guardado con exito!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                        Vista.MoveCurrentToLast();
                        limpiarCampos();
                        break;
                }

                agregaNuevosLIST_USUARIO();
            }
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            btnAlta.Visibility = Visibility.Visible;
            limpiarCampos();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if (esCampoVacio(groupBox1))
            {
                MessageBox.Show("Complete todos los campos", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                Usuario usr = new Usuario();
                usr = TrabajarUsuario.buscarUsuarioNombre(txtUsuario.Text);//retorna usuario si es que existe//
                if (usr != null)
                {
                    if (usr.Usu_ID != int.Parse(txtId.Text))
                    {
                        MessageBox.Show("El nombre usuario: " + txtUsuario.Text + "\n YA EXISTE!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        MessageBoxResult result = MessageBox.Show("Seguro que desea Modificar?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                        switch (result)
                        {
                            case MessageBoxResult.OK:
                                Usuario u = new Usuario();
                                u = setearObjeto();
                                
                                UsuarioRol item = listaUsuarios.FirstOrDefault(i => i.Usu_ID == int.Parse(txtId.Text));
                                if (item != null)
                                {
                                    u.Usu_ID = item.Usu_ID;
                                }
                                 
                                trabajarUsuario.ModificacionUsuario(u);
                                btnAtras_Click(sender, e);
                                btnSiguiente_Click(sender, e);
                                MessageBox.Show("Registro modificado con exito!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                                limpiarCampos();
                                break;
                        }
                    }
                }
                else
                {
                    MessageBoxResult result = MessageBox.Show("Seguro que desea Modificar?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                    switch (result)
                    {
                        case MessageBoxResult.OK:
                            Usuario u = new Usuario();
                            u = setearObjeto();
                            
                            UsuarioRol item = listaUsuarios.FirstOrDefault(i => i.Usu_ID == int.Parse(txtId.Text));
                            if (item != null)
                            {
                                u.Usu_ID = item.Usu_ID;
                            }
                            
                            trabajarUsuario.ModificacionUsuario(u);
                            btnAtras_Click(sender, e);
                            btnSiguiente_Click(sender, e);
                            MessageBox.Show("Registro modificado con exito!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                            limpiarCampos();
                            break;
                    }
                }
                agregaNuevosLIST_USUARIO();
            }
        }

        private void btnBaja_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Seguro que desea ELIMINAR?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.OK:
                    listaUsuarios.Remove(listaUsuarios.SingleOrDefault(i => i.Usu_ID == int.Parse(blockId.Text)));
                    TrabajarUsuario.BajaUsuario(int.Parse(blockId.Text));
                    MessageBox.Show("Registro ELIMINADO!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                    btnAtras_Click(sender, e);
                    break;
            }
            limpiarCampos();
        }
        private void btnSeleccionar_Click(object sender, RoutedEventArgs e)
        {
            btnAlta.Visibility = Visibility.Hidden;
            btnModificar.Visibility = Visibility.Visible;
            Usuario u = new Usuario();
            u = TrabajarUsuario.buscarUsuarioId(int.Parse(blockId.Text));
            txtId.Text = u.Usu_ID.ToString();
            txtUsuario.Text = u.Usu_NombreUsuario;
            txtPassword.Text = u.Usu_Password;
            txtName.Text = u.Usu_ApellidoNombre;
            Rol rolU = listaRol.Find(r=>r.Rol_Codigo == u.Rol_Codigo);
            if (rolU != null) {
                cmbRol.Text = rolU.Rol_Descripcion;
            }
            //button_Guardar.Visibility = Visibility.Hidden;
        }

        private void btnAtras_Click(object sender, RoutedEventArgs e)
        {
            Vista.MoveCurrentToPrevious();
            if (Vista.IsCurrentBeforeFirst)
            {
                Vista.MoveCurrentToLast();
            }
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            Vista.MoveCurrentToNext();
            if (Vista.IsCurrentAfterLast)
            {
                Vista.MoveCurrentToFirst();
            }
        }

        private void btnPrimero_Click(object sender, RoutedEventArgs e)
        {
            Vista.MoveCurrentToFirst();
        }

        private void btnUltimo_Click(object sender, RoutedEventArgs e)
        {
            Vista.MoveCurrentToLast();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            winUsuarioLista window = new winUsuarioLista();
            window.ShowDialog();
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        /// <summary>
        /// ----------------FUNCIONES INTERNAS-----------
        /// </summary>
        /// <returns></returns>

        private Usuario setearObjeto()
        {
            Usuario user = new Usuario();
            user.Usu_NombreUsuario = txtUsuario.Text;
            user.Usu_Password = txtPassword.Text;
            user.Usu_ApellidoNombre = txtName.Text;
            Rol rolU = listaRol.Find(r=> r.Rol_Descripcion == cmbRol.Text);
            if (rolU != null)
            {
                user.Rol_Codigo = rolU.Rol_Codigo;
            }
            return user;
        }

        private bool esCampoVacio(GroupBox gp)
        {
            bool vacio = false;
            IEnumerable<Control> listaFormulario = ((Grid)gp.Content).Children.OfType<Control>() ;
            foreach (Control oControls in listaFormulario)
            {
                if (oControls is TextBox)
                {
                    if (((TextBox)oControls).Text == "")
                    {
                        vacio = true;
                    }
                }
            }
            return vacio;
        }

        public void limpiarCampos()
        {
            txtId.Text = "";
            txtUsuario.Text = "";
            txtPassword.Text = "";
            txtName.Text = "";
           cmbRol.Text = "";
        }
        private void agregaNuevosLIST_USUARIO()
        {
            ObservableCollection<UsuarioRol> usuariosRol = trabajarUsuario.traerUsuario();

            foreach (var usuarioRolBD in usuariosRol)
            {
                UsuarioRol usuarioRol = listaUsuarios.ToList().Find(i=>i.Usu_ID== usuarioRolBD.Usu_ID);
                if (usuarioRol == null)
                {
                        listaUsuarios.Add(usuarioRolBD);
                    
                } else {
                    int x =listaUsuarios.IndexOf(usuarioRol);
                    listaUsuarios[x]= usuarioRolBD;
                }
            }

        }
    }
}
