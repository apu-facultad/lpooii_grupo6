﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace Vistas
{
    /// <summary>
    /// Interaction logic for WinAudio.xaml
    /// </summary>
    public partial class WinAudio : Window
    {
        public WinAudio()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            var fadeAnimation = new DoubleAnimation();
            fadeAnimation.From = 1;
            fadeAnimation.To = 0;
            fadeAnimation.AutoReverse = true;
            stpGrupo.BeginAnimation(Label.OpacityProperty, fadeAnimation);
            tituloCine.BeginAnimation(Label.OpacityProperty, fadeAnimation);
            stpIntegrantes.BeginAnimation(Label.OpacityProperty, fadeAnimation);

        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
                  
            this.Close();
        }
    }
}
