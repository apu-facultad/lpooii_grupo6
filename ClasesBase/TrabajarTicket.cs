﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace ClasesBase
{
    public class TrabajarTicket
    {
        static SqlConnection conexion = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
        private Ticket ticket = new Ticket();

        public Ticket Ticket
        {
            get { return ticket; }
            set { ticket = value; }
        }
        private Butaca butaca = new Butaca();

        public Butaca Butaca
        {
            get { return butaca; }
            set { butaca = value; }
        }
        
        public void cargarButaca(string fila, string nro) {
            butaca.But_Fila = fila;
            butaca.But_Numero = nro;
        }

        public Butaca getButaca() {
            return butaca;
        }

        static public void AltaTicket(Ticket ticket)
        {
            conexion.Open();
            SqlCommand comando = new SqlCommand("insertarTicket");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Connection = conexion;

            comando.Parameters.AddWithValue("@fecha", ticket.Tick_FechaVenta);
            comando.Parameters.AddWithValue("@dni", ticket.Cli_Dni);
            comando.Parameters.AddWithValue("@proy", ticket.Proy_codigo);
            //comando.Parameters.AddWithValue("@fila", ticket.But_fila);
            comando.Parameters.AddWithValue("@butaca", ticket.But_codigo);
            comando.Parameters.AddWithValue("@codigoTicket", ticket.Tick_Nro);
            comando.Parameters.AddWithValue("@precio", ticket.Tick_precio);
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        static public int getTicketsCount()
        {
            DataTable dt = traerTicket();
            List<Ticket> tickets = ModelSchema.CreateListFromTable<Ticket>(dt);
            return tickets.Count;
        }

        public static DataTable traerTicket()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "listarTickets";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = conexion;

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);
            return dt;
        }

        /** Extrae Ticket por butaca de la base de datos por sala
         */
        static public DataTable selectTickets(int codigo_proyeccion)
        {
            Usuario usuario = new Usuario();
            conexion.Open();
            SqlCommand comando = new SqlCommand("buscarTicket");
            comando.CommandType = CommandType.StoredProcedure;
            comando.Connection = conexion;
            comando.Parameters.AddWithValue("codigo_proyeccion", codigo_proyeccion);
            DataTable dataTable = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(comando);
            dataAdapter.Fill(dataTable);
            conexion.Close();
            return dataTable;
        }

/** Deprecado */
        static public List<Ticket> getTicketsForProyeccion(int codigo_proyeccion)
        {
            DataTable dt = TrabajarTicket.selectTickets(codigo_proyeccion);
            List<Ticket> tickets = ModelSchema.CreateListFromTable<Ticket>(dt);

            if (tickets.Count == 0)
            {
                return null;
            }
            return tickets;
        }
        
        /*******************************/
        public Ticket getTicketCliente(int nroProyeccion, int codButaca)//cambiar despues a int
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Ticket WHERE proy_codigo = @proyeccion and but_codigo= @cod";
            cmd.Parameters.AddWithValue("@proyeccion", nroProyeccion);
            cmd.Parameters.AddWithValue("@cod", codButaca);
            //cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            conexion.Close();
            if (dt.Rows.Count == 0)
            {
                return null;//
            }
            else
            { 
                /*
                foreach (DataRow row in dt.Rows)
                {
                    dni = row["cli_dni"].ToString();
                } */
                return new Ticket();
            }
        }
        
        /*******************************/
        public string traerButacaCliente(string codigo)//cambiar despues a int
        {
            string dni = ""; //cambiar a int despues
            SqlConnection conexion = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Ticket WHERE but_numero= @codigo"; //cambiar despues a but_codigo
            cmd.Parameters.AddWithValue("@codigo", codigo);
            //cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return "";//
            }
            else
            {
                foreach (DataRow row in dt.Rows)
                {
                    dni = row["cli_dni"].ToString();
                }
                return dni;
            }
        }

        public int traerCodigoButaca(string fila, string numero) {
            int codigo = 0;
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Butaca WHERE but_numero = @numero and but_fila= @fila";
            cmd.Parameters.AddWithValue("@numero",numero );
            cmd.Parameters.AddWithValue("@fila", fila);
            //cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            conexion.Close();
            if (dt.Rows.Count == 0)
            {
            }
            else
            {
                foreach (DataRow row in dt.Rows)
                {
                    codigo = (int)row["but_codigo"];
                }
            }
            return codigo;
        }
    }
}
