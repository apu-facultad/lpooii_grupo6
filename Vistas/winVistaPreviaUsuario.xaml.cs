﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;
using ClasesBase.dtos;

namespace Vistas
{
    /// <summary>
    /// Interaction logic for winVistaPreviaUsuario.xaml
    /// </summary>
    public partial class winVistaPreviaUsuario : Window
    {
        CollectionViewSource fil = new CollectionViewSource(); //vista de coleccion filtrada

        public winVistaPreviaUsuario(CollectionViewSource coleccion)
        {
            InitializeComponent();
            fil = coleccion;
            InitializeComponent();
            MessageBox.Show("TOTAL USUARIOS: " + fil.View.Cast<UsuarioRol>().Count());
            lstUsuario.ItemsSource = fil.View;
        }

        private void button_Imprimir_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog pdlg = new PrintDialog();
            if (pdlg.ShowDialog() == true)
            {
                pdlg.PrintDocument(((IDocumentPaginatorSource)DocMain).DocumentPaginator, "Imprimir");
            }
        }
    }
}
