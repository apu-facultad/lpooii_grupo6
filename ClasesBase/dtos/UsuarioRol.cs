﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase.dtos
{
    public class UsuarioRol
    {
        private int usu_ID;
        private string usu_Password;
        private string usu_NombreUsuario;
        private string usu_ApellidoNombre;
        private string rol_description;

        public int Usu_ID
        {
            get { return usu_ID; }
            set { usu_ID = value; }
        }
        public string Usu_NombreUsuario
        {
            get { return usu_NombreUsuario; }
            set { usu_NombreUsuario = value; }
        }
        public string Usu_Password
        {
            get { return passwordOculta(usu_Password); }
            set { usu_Password = value; }
        }
        public string Usu_ApellidoNombre
        {
            get { return usu_ApellidoNombre; }
            set { usu_ApellidoNombre = value; }
        }
        public string Rol_description
        {
            get { return rol_description; }
            set { rol_description = value; }
        }

        private string passwordOculta (string password) 
        {
            string asterisco = "";
            for (int i = 0; i < password.Length; i++)
            {
                asterisco += "*";
            }
            return asterisco;
        }
    }
}
