﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Vistas
{
    /// <summary>
    /// Interaction logic for controlLogin.xaml
    /// </summary>
    public partial class controlLogin : UserControl
    {

        public controlLogin()
        {
            InitializeComponent();
        }

        public String Usuario
        {
            get {return txtUsuario.Text;}
            set { txtUsuario.Text = value;  }
        }

        public String Password
        {
            get { return txtContra.Password; }
            set { txtContra.Password = value; }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
