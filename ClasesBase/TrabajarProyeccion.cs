﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ClasesBase
{

    public class TrabajarProyeccion
    {
        static SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);

        // para el abm proyecciones
        public static DataTable traerProyecciones()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT * FROM Proyeccion";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;
            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dt);
            return dt;
        }

        // para el listado de proyecciones filtrado por fecha
        public static DataTable traerProyecciones2()
        {

            DateTime fyhora = DateTime.Today;
            string finicio = fyhora.ToString("dd/MM/yyyy");
            DateTime nuevaFecha = DateTime.Today;
            nuevaFecha = nuevaFecha.AddDays(7);
            string ffinal = nuevaFecha.ToString("dd/MM/yyyy");
            SqlCommand cmd = new SqlCommand();
            /*
            // Rama fix-stored
            cmd.CommandText = "listarProyecciones";
            cmd.CommandType = CommandType.StoredProcedure;
            */

            // cmd.CommandText = "SELECT * FROM VistaProyeccionPelicula where proy_fecha between @finicio and @ffinal and peli_titulo = @peli";

            //cmd.CommandText = "SELECT * FROM Proyeccion where CONVERT (datetime, proy_fecha, 20) between @finicio and @ffinal";
            cmd.CommandText = "SELECT * FROM vistaProyecciones where proy_fecha between @finicio and @ffinal";
            cmd.CommandType = CommandType.Text;

            cmd.Connection = cn;
            cmd.Parameters.AddWithValue("@finicio", finicio);
            cmd.Parameters.AddWithValue("@ffinal", ffinal);
            // cmd.Parameters.AddWithValue("@peli", peli);

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);
            return dt;
        }




       

        //public static DataTable traerProyecciones() {           
            
        //    SqlCommand cmd = new SqlCommand();
            
        //    cmd.CommandText = "SELECT * FROM vistaProyecciones";
        //    cmd.CommandType = CommandType.Text;

        //    cmd.Connection = cn;

        //    DataTable dt = new DataTable();
        //    SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

        //    dataAdapter.Fill(dt);
        //    return dt;
        //}

        static public DataTable selectProyecciones() {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "listarProyecciones";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);
            return dt;
        }

        public static List<Proyeccion> getListaProyecciones()
        {
            // ObservableCollection<Proyeccion> collectionUsuario = new ObservableCollection<Proyeccion>();
            DataTable dt = TrabajarProyeccion.traerProyecciones();
            List<Proyeccion> proyeccion = ModelSchema.CreateListFromTable<Proyeccion>(dt);
            /* if (usuarios.Count == 0)
            {
                return null;
            }
            foreach (Proyeccion user in usuarios)
            {
                collectionUsuario.Add(user);
            } */
            return proyeccion;
        }

        public static Proyeccion obtenerProy(int cod)
        {
            Proyeccion proy = new Proyeccion();
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "buscarProyeccion";
            
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@cod", cod);
            cmd.Connection = cn;

            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                string var = dt.Rows[0]["proy_codigo"].ToString();
                proy.Proy_Codigo = Convert.ToInt32(var);
                proy.Proy_Fecha = dt.Rows[0]["proy_fecha"].ToString();
                proy.Proy_Hora = dt.Rows[0]["proy_hora"].ToString();
                proy.Sala_Numero = int.Parse(dt.Rows[0]["sala_numero"].ToString());
                proy.Peli_Codigo = dt.Rows[0]["peli_codigo"].ToString();
                return proy;
            }
        }

        public static void insertarProyeccion(Proyeccion proy)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "insertarProyeccion";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@codigo", proy.Proy_Codigo);
            cmd.Parameters.AddWithValue("@fecha", proy.Proy_Fecha);
            cmd.Parameters.AddWithValue("@hora", proy.Proy_Hora);
            cmd.Parameters.AddWithValue("@sala", proy.Sala_Numero);
            cmd.Parameters.AddWithValue("@peli", proy.Peli_Codigo);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void modificarProyeccion(Proyeccion proy)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "modificarProyeccion";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@codigo", proy.Proy_Codigo);
            cmd.Parameters.AddWithValue("@fecha", proy.Proy_Fecha);
            cmd.Parameters.AddWithValue("@hora", proy.Proy_Hora);
            cmd.Parameters.AddWithValue("@sala", proy.Sala_Numero);
            cmd.Parameters.AddWithValue("@peli", proy.Peli_Codigo);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        static public int getProyCount()
        {
            DataTable dt = traerProyecciones();
            List<Proyeccion> proyecciones = ModelSchema.CreateListFromTable<Proyeccion>(dt);
            return proyecciones.Count;
        }

        public static void eliminarProyeccion(int cod)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "eliminarProyeccion";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.AddWithValue("@codP", cod);

            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }
    }
}
