﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace ClasesBase
{
    public class ModelSchema
    {
        /**
         * Referencia https://stackoverflow.com/questions/8008389/how-to-convert-datatable-to-class-object
         */
        // function that creates a list of an object from the given data table
        public static List<T> CreateListFromTable<T>(DataTable tbl) where T : new()
        {
            // define return list
            List<T> lst = new List<T>();

            // go through each row
            foreach (DataRow r in tbl.Rows)
            {
                // add to the list
                lst.Add(CreateItemFromRow<T>(r));
            }

            // return the list
            return lst;
        }

        // function that creates an object from the given data row
        public static T CreateItemFromRow<T>(DataRow row) where T : new()
        {
            // create a new object
            T item = new T();

            // set the item
            SetItemFromRow(item, row);

            // return 
            return item;
        }
        public static void SetItemFromRow<T>(T item, DataRow row) where T : new()
        {
            // go through each column
            foreach (DataColumn c in row.Table.Columns)
            {
                // // find the property for the column
                // PropertyInfo p = item.GetType().GetProperty(c.ColumnName);

                // if exists, set the value
                if (item.GetType().GetProperty(c.ColumnName) != null && row[c] != DBNull.Value)
                {
                    item.GetType().GetProperty(c.ColumnName).SetValue(item, row[c], null);
                }
            }
        }
    }
}
