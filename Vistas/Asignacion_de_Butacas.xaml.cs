﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Interaction logic for Asignacion_de_Butacas.xaml
    /// </summary>
    public partial class Asignacion_de_Butacas : Window
    {
        Ticket tick = new Ticket();
        Butaca butt = new Butaca();
        public Asignacion_de_Butacas()
        {
            InitializeComponent();
        }
        private Brush CambiarColor(Brush brush)
        {
            Brush cambio;

            if (brush.Equals(Brushes.LightGray))
            {
                cambio = Brushes.Green;
                return cambio;
            }
            else if (brush.Equals(Brushes.Green))
            {
                cambio = Brushes.LightGray;
                return cambio;
            }
            else
            {
                MessageBox.Show("Butaca no disponible");
                cambio = Brushes.Red;
                return cambio;
            }
        }

        private void btnA1_Click(object sender, RoutedEventArgs e)
        {
            btnA1.Background = CambiarColor(btnA1.Background);

            //winTicket win = new winTicket();
            TrabajarTicket tt = new TrabajarTicket();
            tt.cargarButaca("a","1");
            //win.ShowDialog();
            Close();
        }

        private void btnA2_Click(object sender, RoutedEventArgs e)
        {
            btnA2.Background = CambiarColor(btnA2.Background);
        }

        private void btnA3_Click(object sender, RoutedEventArgs e)
        {
            btnA3.Background = CambiarColor(btnA3.Background);
        }

        private void btnA4_Click(object sender, RoutedEventArgs e)
        {
            btnA4.Background = CambiarColor(btnA4.Background);
        }

        private void btnB1_Click(object sender, RoutedEventArgs e)
        {
            btnB1.Background = CambiarColor(btnB1.Background);
        }

        private void btnB2_Click(object sender, RoutedEventArgs e)
        {
            btnB2.Background = CambiarColor(btnB2.Background);
        }

        private void btnB3_Click(object sender, RoutedEventArgs e)
        {
            btnB3.Background = CambiarColor(btnB3.Background);
        }

        private void btnB4_Click(object sender, RoutedEventArgs e)
        {
            btnB4.Background = CambiarColor(btnB4.Background);
        }

        private void btnC1_Click(object sender, RoutedEventArgs e)
        {
            btnC1.Background = CambiarColor(btnC1.Background);
        }

        private void btnC2_Click(object sender, RoutedEventArgs e)
        {
            btnC2.Background = CambiarColor(btnC2.Background);
        }

        private void btnC3_Click(object sender, RoutedEventArgs e)
        {
            btnC3.Background = CambiarColor(btnC3.Background);
        }

        private void btnC4_Click(object sender, RoutedEventArgs e)
        {
            btnC4.Background = CambiarColor(btnC4.Background);
        }

        private void btnD1_Click(object sender, RoutedEventArgs e)
        {
            btnD1.Background = CambiarColor(btnD1.Background);
        }

        private void btnD2_Click(object sender, RoutedEventArgs e)
        {
            btnD2.Background = CambiarColor(btnD2.Background);
        }

        private void btnD3_Click(object sender, RoutedEventArgs e)
        {
            btnD3.Background = CambiarColor(btnD3.Background);
        }

        private void btnD4_Click(object sender, RoutedEventArgs e)
        {
            btnD4.Background = CambiarColor(btnD4.Background);
        }

        private void btnE1_Click(object sender, RoutedEventArgs e)
        {
            btnE1.Background = CambiarColor(btnE1.Background);
        }

        private void btnE2_Click(object sender, RoutedEventArgs e)
        {
            btnE2.Background = CambiarColor(btnE2.Background);
        }

        private void btnE3_Click(object sender, RoutedEventArgs e)
        {
            btnE3.Background = CambiarColor(btnE3.Background);
        }

        private void btnE4_Click(object sender, RoutedEventArgs e)
        {
            btnE4.Background = CambiarColor(btnE4.Background);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            btnB3.Background = Brushes.Red;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Su Eleccion es:");
            this.Close();
        }

    }
}
