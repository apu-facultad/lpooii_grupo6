﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase
{
    public class Ticket
    {

        public Ticket(int nro, DateTime fecha, int dni, int proy,int butaca, float precio) {
            this.tick_Nro = nro;
            this.tick_FechaVenta = fecha;
            this.cli_Dni = dni;
            this.proy_codigo = proy;
            this.but_codigo = butaca;
            this.tick_precio = precio;
        }

        public Ticket() { }

        private int tick_Nro;

        public int Tick_Nro
        {
            get { return tick_Nro; }
            set { tick_Nro = value; }
        }
        private DateTime tick_FechaVenta;

        public DateTime Tick_FechaVenta
        {
            get { return tick_FechaVenta; }
            set { tick_FechaVenta = value; }
        }

        private int cli_Dni;

        public int Cli_Dni
        {
            get { return cli_Dni; }
            set { cli_Dni = value; }
        }
        private int proy_codigo;

        public int Proy_codigo
        {
            get { return proy_codigo; }
            set { proy_codigo = value; }
        }

        //hay que sacar but fila y but numero
        private string but_fila;

        public string But_fila
        {
            get { return but_fila; }
            set { but_fila = value; }
        }
        private string but_numero;

        public string But_numero
        {
            get { return but_numero; }
            set { but_numero = value; }
        }

        private int but_codigo;

        public int But_codigo
        {
            get { return but_codigo; }
            set { but_codigo = value; }
        }
        private float tick_precio;

        public float Tick_precio
        {
            get { return tick_precio; }
            set { tick_precio = value; }
        }
    }
}
