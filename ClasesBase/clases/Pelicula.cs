﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;

namespace ClasesBase
{
    public class Pelicula : IDataErrorInfo
    {
        public Pelicula()
        {
        }

        public Pelicula(string codigo, string titulo, string duracion, string genero, string clase)
        {
            this.peli_Clase = clase;
            this.peli_Codigo = codigo;
            this.peli_Duracion = duracion;
            this.peli_Genero = genero;
            this.peli_Titulo = titulo;
        }

        public Pelicula(string codigo, string titulo, string duracion, string genero, string clase, string imagen, string avance)
        {
            this.peli_Clase = clase;
            this.peli_Codigo = codigo;
            this.peli_Duracion = duracion;
            this.peli_Genero = genero;
            this.peli_Titulo = titulo;
            this.peli_avance = avance;
            this.peli_imagen = imagen;
        }

        private string peli_imagen;

        public string Peli_imagen
        {
            get { return peli_imagen; }
            set { peli_imagen = value; }
        }
        private string peli_avance;

        public string Peli_avance
        {
            get { return peli_avance; }
            set { peli_avance = value; }
        }

        private string peli_Codigo;

        private string peli_Titulo;

        private string peli_Duracion;

        private string peli_Genero;

        private string peli_Clase;


        public string Peli_Titulo
        {
            get { return peli_Titulo; }
            set { peli_Titulo = value; }
        }

        public string Peli_Duracion
        {
            get { return peli_Duracion; }
            set { peli_Duracion = value; }
        }
        public string Peli_Genero
        {
            get { return peli_Genero; }
            set { peli_Genero = value; }
        }

        public string Peli_Codigo
        {
            get { return peli_Codigo; }
            set { peli_Codigo = value; }
        }
        public string Peli_Clase
        {
            get { return peli_Clase; }
            set { peli_Clase = value; }
        }



        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                string mensaje = string.Empty;
                //validacion //
                if (columnName == "Peli_Titulo")
                {
                    if (string.IsNullOrEmpty(Peli_Titulo))
                    {
                        mensaje = "Titulo de pelicula obligatorio..";
                    }

                }
                if (columnName == "Peli_Duracion")
                {
                    if (string.IsNullOrEmpty(Peli_Duracion))
                    {
                        mensaje = "Duracion de pelicula obligatorio..";
                    }

                }
                //fin validacion//

                return mensaje;
            }
        }
            
    }
}
