﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Interaction logic for WinVentaTickets.xaml
    /// </summary>
    public partial class WinVentaTickets : Window
    {
        public WinVentaTickets(Butaca butaca, Cliente cliente, Proyeccion proy, Ticket ticket, Usuario user)
        {
            InitializeComponent();
            txtFecha.Text = "Fecha: "+DateTime.Now.ToLongDateString() + "Hora: " + DateTime.Now.ToLongTimeString();
            txtNumero.Text = ticket.Tick_Nro.ToString();
            txtProy.Text = proy.Proy_Codigo.ToString();
            txtButaca.Text = butaca.But_Numero;
            txtFila.Text = butaca.But_Fila;
            txtDni.Text = cliente.Cli_DNI.ToString();
            txtApellido.Text = cliente.Cli_Apellido;
            txtNombre.Text = cliente.Cli_Nombre;
            lblVend.Content = user.Usu_ApellidoNombre;
        }
    }
}
