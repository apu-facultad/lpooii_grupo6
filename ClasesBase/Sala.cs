﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase
{
    public class Sala
    {
        private int sala_numero;

        public int Sala_numero
        {
            get { return sala_numero; }
            set { sala_numero = value; }
        }
        private string sala_denominacion;

        public string Sala_denominacion
        {
            get { return sala_denominacion; }
            set { sala_denominacion = value; }
        }
        private int sala_capacidad;

        public int Sala_capacidad
        {
            get { return sala_capacidad; }
            set { sala_capacidad = value; }
        }
    }
}
