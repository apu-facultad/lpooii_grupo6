﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClasesBase.clases
{
    class Sala
    {
        private int sala_Numero;

        public int Sala_Numero
        {
            get { return sala_Numero; }
            set { sala_Numero = value; }
        }
        private string sala_Denominacion;

        public string Sala_Denominacion
        {
            get { return sala_Denominacion; }
            set { sala_Denominacion = value; }
        }
        private int sala_Capacidad;

        public int Sala_Capacidad
        {
            get { return sala_Capacidad; }
            set { sala_Capacidad = value; }
        }

    }
}
