﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;

namespace Vistas
{
    /// <summary>
    /// Lógica de interacción para winMain.xaml
    /// </summary>
    public partial class winMain : Window
    {
        private Usuario usuarioLogeado;

        public winMain()
        {
            InitializeComponent();
        }

        private void itemUsuario_Click(object sender, RoutedEventArgs e)
        {
            winUsuario window = new winUsuario();
            window.ShowDialog();
        }

        private void itemPelicula_Click(object sender, RoutedEventArgs e)
        {
            winPelicula window = new winPelicula();
            window.ShowDialog();
        }

        private void itemProyeccion_Click(object sender, RoutedEventArgs e)
        {
            winProyeccion window = new winProyeccion();
            window.ShowDialog();
        }

        private void itemButaca_Click(object sender, RoutedEventArgs e)
        {
            Asignacion_de_Butacas window = new Asignacion_de_Butacas();
            window.ShowDialog();
        }

        private void itemCliente_Click(object sender, RoutedEventArgs e)
        {
            winCliente window = new winCliente();
            window.ShowDialog();
        }

        private void itemTicket_Click(object sender, RoutedEventArgs e)
        {
            //cambiar el metodo para mandar la butaca desde asignacion butaca
            //Butaca butaca = new Butaca("a","2","sala1");
            winTicket ticket = new winTicket(this.usuarioLogeado);
            ticket.Show();
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            Close();
            itemUsuario.Visibility = System.Windows.Visibility.Hidden;
            itemCliente.Visibility = System.Windows.Visibility.Hidden;
            itemPelicula.Visibility = System.Windows.Visibility.Hidden;
            itemProyeccion.Visibility = System.Windows.Visibility.Hidden;
            itemTicket.Visibility = System.Windows.Visibility.Hidden;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            itemUsuario.Visibility = System.Windows.Visibility.Hidden;
            itemCliente.Visibility = System.Windows.Visibility.Hidden;
            itemPelicula.Visibility = System.Windows.Visibility.Hidden;
            itemProyeccion.Visibility = System.Windows.Visibility.Hidden;
            itemTicket.Visibility = System.Windows.Visibility.Hidden;

            controlarRol();
        }

        private void controlarRol()
        {
            int idRol = this.usuarioLogeado.Rol_Codigo;
            if (idRol == 1)
            {
                itemUsuario.Visibility = System.Windows.Visibility.Visible;
                itemPelicula.Visibility = System.Windows.Visibility.Visible;
                itemProyeccion.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                if (idRol == 2)
                {
                    itemCliente.Visibility = System.Windows.Visibility.Visible;
                    itemTicket.Visibility = System.Windows.Visibility.Visible;
                }

            }
        }

        public Usuario UsuarioLogeado
        {
            get { return usuarioLogeado; }
            set { usuarioLogeado = value; }
        }

        private void itemTicketLista_Click(object sender, RoutedEventArgs e)
        {
            WinTicketLista win = new WinTicketLista();
            win.ShowDialog();
        }

    }
}
