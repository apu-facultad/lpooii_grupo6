﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace ClasesBase
{
    public class TrabajarSala
    {
        static SqlConnection cn = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);

        public void altaSala(Sala sala)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO Sala(sala_numero, sala_denominacion, sala_capacidad) values(@nro, @denominacion, @capacidad)";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;
            cmd.Parameters.AddWithValue("@sala_numero", sala.Sala_numero);
            cmd.Parameters.AddWithValue("@sala_denominacion", sala.Sala_denominacion);
            cmd.Parameters.AddWithValue("@sala_capacidad", sala.Sala_capacidad);
            cn.Open();
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public void bajaSala(int codigo)
        {
            SqlConnection conexion = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            SqlCommand comando = new SqlCommand();
            comando.CommandText = "DELETE FROM Sala WHERE sala_numero = '" + codigo + "'";
            comando.CommandType = CommandType.Text;
            comando.Connection = conexion;
            conexion.Open();
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        public void modificarSala(Sala sala)
        {
            SqlConnection conexion = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            SqlCommand comando = new SqlCommand("UPDATE Sala SET sala_numero = @nro, sala_denominacion = @denominacion, sala_capacidad = @capacidad WHERE sala_numero = '" + sala.Sala_numero + "'", conexion);
            comando.Parameters.AddWithValue("@nro", sala.Sala_numero);
            comando.Parameters.AddWithValue("@denominacion", sala.Sala_denominacion);
            comando.Parameters.AddWithValue("@capacidad", sala.Sala_capacidad);
            conexion.Open();
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        public DataTable listarSala()
        {
            SqlConnection conexion = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            conexion.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT sala_denominacion AS DATOS FROM Sala ORDER BY sala_numero";
            cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;

            DataTable dt = new DataTable();
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

            dataAdapter.Fill(dt);

            return dt;
        }

        public int traerSalaTipo(string tipo)
        {
            int aux = 0;
            SqlConnection conexion = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            conexion.Open();

            SqlCommand cmd = new SqlCommand("SELECT * FROM Sala WHERE sala_numero = @tipo", conexion);
            cmd.Parameters.AddWithValue("@tipo", tipo);

            //cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                foreach (DataRow row in dt.Rows)
                {
                    aux = (int)row["sala_capacidad"];
                }
                return aux;
            }
        }

        public int traerSalaCodigo(string tipo)
        {
            int aux = 0;
            SqlConnection conexion = new SqlConnection(ClasesBase.Properties.Settings.Default.conexion);
            conexion.Open();

            SqlCommand cmd = new SqlCommand("SELECT * FROM Sala WHERE sala_denominacion = @tipo", conexion);
            cmd.Parameters.AddWithValue("@tipo", tipo);

            //cmd.CommandType = CommandType.Text;
            cmd.Connection = conexion;
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                foreach (DataRow row in dt.Rows)
                {
                    aux = (int)row["sala_numero"];
                }
                return aux;
            }
        }

        public string getNombreSala(int cod) {
            string aux = "";
            cn.Open();

            SqlCommand cmd = new SqlCommand("SELECT * FROM Sala WHERE sala_numero = @tipo", cn);
            cmd.Parameters.AddWithValue("@tipo", cod);

            //cmd.CommandType = CommandType.Text;
            cmd.Connection = cn;
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            cn.Close();
            if (dt.Rows.Count == 0)
            {
            }
            else
            {
                foreach (DataRow row in dt.Rows)
                {
                    aux = row["sala_denominacion"].ToString();
                }
            }

            return aux;
        }
    }
}
