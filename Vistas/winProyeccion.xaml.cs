﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;
using System.Data; 

namespace Vistas
{
    /// <summary>
    /// Interaction logic for winProyeccion.xaml
    /// </summary>
    public partial class winProyeccion : Window
    {
        TrabajarSala trabajarSala = new TrabajarSala();
        TrabajarPelicula trabajarPelicula = new TrabajarPelicula();
        Proyeccion selectProyeccion = new Proyeccion();
        public winProyeccion()
        {
            InitializeComponent();
            cargarComboSala();
            cargarComboPelicula();
            cargarLista();
            limpiar();
        }

        private void cargarComboSala()
        {
            cmbSala.DisplayMemberPath = "DATOS";
            cmbSala.SelectedValuePath = "sala_nro";
            cmbSala.ItemsSource = trabajarSala.listarSala().DefaultView;
            cmbSala.SelectedValue = 1;
        }

        private void cargarComboPelicula()
        {
            cmbCodPelicula.DisplayMemberPath = "DATOS";
            cmbCodPelicula.SelectedValuePath = "peli_codigo";
            cmbCodPelicula.ItemsSource = trabajarPelicula.listarPelicula().DefaultView;
            cmbCodPelicula.SelectedValue = 1;
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if ((cmbCodPelicula.Text != "") && (cmbSala.Text != "") && (txtHora.Text != "") && (dateFecha.Text != ""))
            {
                MessageBoxResult result = MessageBox.Show("¿Desea agregar proyección?", "Agregar Cliente", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    int cantidadProyeccionMore = TrabajarProyeccion.getProyCount() + 1;
                    int codigoSala = trabajarSala.traerSalaCodigo(cmbSala.Text);
                    string codigoPelicula = trabajarPelicula.traerCodigoPelicula(cmbCodPelicula.Text);
                    Proyeccion proyecion = new Proyeccion(cantidadProyeccionMore, dateFecha.ToString(),txtHora.Text,codigoSala,codigoPelicula);
                    
                    TrabajarProyeccion.insertarProyeccion(proyecion);
                    limpiar();
                    cargarLista();
                }
                else
                {

                }
            }
            else
            {
                MessageBox.Show("Ingrese todos los campos");
            }
        }

        private void cargarLista()
        {
            List<Proyeccion> listProyecc = new List<Proyeccion>();
            DataTable dt = TrabajarProyeccion.traerProyecciones();
            foreach (DataRow row in dt.Rows)
            {
                Proyeccion proy = new Proyeccion();
                proy.Proy_Codigo = int.Parse(row["proy_codigo"].ToString());
                proy.Proy_Fecha = row["proy_fecha"].ToString();
                proy.Proy_Hora = row["proy_hora"].ToString();
                proy.Sala_Numero = int.Parse(row["sala_numero"].ToString());
                proy.Peli_Codigo = row["peli_codigo"].ToString();
                listProyecc.Add(proy);
            }
            gridProyec.ItemsSource = listProyecc;
        }

        private void limpiar()
        {
            cmbCodPelicula.Text = "";
            txtHora.Text = "";
            cmbSala.Text = "";
            dateFecha.Text = "";
            btnAgregar.Visibility = Visibility.Visible;
            btnEliminar.Visibility = Visibility.Hidden;
            btnModificar.Visibility = Visibility.Hidden;
        }



        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnModificar_Click(object sender, RoutedEventArgs e)
        {
            if ((cmbCodPelicula.Text == "") && (txtHora.Text == "") && (cmbSala.Text == "") && (dateFecha.Text == ""))
            {
                MessageBox.Show("Complete todos los campos", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("Seguro que desea MODIFICAR?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
                switch (result)
                {
                    case MessageBoxResult.OK:
                        Proyeccion proy = new Proyeccion();
                        string nombrePelicula = cmbCodPelicula.Text;
                        string codigoPelicula = trabajarPelicula.traerCodigoPelicula(nombrePelicula);
                        Pelicula pelicula = TrabajarPelicula.buscarPeliculaByCodigo(codigoPelicula);
                        proy.Proy_Codigo = selectProyeccion.Proy_Codigo;
                        proy.Peli_Codigo = pelicula.Peli_Codigo;
                        proy.Proy_Fecha = dateFecha.Text;
                        proy.Proy_Hora = txtHora.Text;
                        int salaNumero = trabajarSala.traerSalaCodigo(cmbSala.Text);
                        proy.Sala_Numero = salaNumero;
                        TrabajarProyeccion.modificarProyeccion(proy);
                        cargarLista();
                        limpiar();
                        break;
                }
            }
        }

        private void bntEliminar_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Seguro que desea ELIMINAR?", "ATENCION", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            switch (result)
            {
                case MessageBoxResult.OK:
                    Proyeccion proy = (Proyeccion)this.gridProyec.SelectedItem;
                    TrabajarProyeccion.eliminarProyeccion(proy.Proy_Codigo);
                    cargarLista();
                    limpiar();
                    MessageBox.Show("Registro ELIMINADO!", "ATENCION", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
            }
            limpiar();
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            limpiar();
        }

        private void gridProyec_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnModificar.Visibility = Visibility.Visible;
            btnEliminar.Visibility = Visibility.Visible;
            btnAgregar.Visibility = Visibility.Hidden;
            Proyeccion proy = new Proyeccion();
            proy = (Proyeccion)this.gridProyec.SelectedItem;
            if (proy != null)
            {
                cmbCodPelicula.Text = proy.Proy_Codigo.ToString();
                dateFecha.Text = proy.Proy_Fecha;
                txtHora.Text = proy.Proy_Hora;
                string sala = trabajarSala.getNombreSala(proy.Sala_Numero);
                Pelicula peli = TrabajarPelicula.buscarPeliculaByCodigo(proy.Peli_Codigo);
                cmbSala.Text = sala;
                cmbCodPelicula.Text = peli.Peli_Titulo;

                selectProyeccion = proy;
            }
        }
    } 
   }

