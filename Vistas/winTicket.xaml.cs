﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClasesBase;
using System.Data;
using System.Text.RegularExpressions;

namespace Vistas
{
    /// <summary>
    /// Interaction logic for winTicket.xaml
    /// </summary>
    public partial class winTicket : Window
    {
        private char gSEPARADOR_BUTACA = ' ';

        private Butaca but = new Butaca();
        private TrabajarCliente tc = new TrabajarCliente();
        private TrabajarProyeccion trabajarProyeccion = new TrabajarProyeccion();
        private TrabajarPelicula trabajarPelicula = new TrabajarPelicula();
        private TrabajarTicket trabajarTicket = new TrabajarTicket();
        private TrabajarSala trabajarSala = new TrabajarSala();
        private List<Proyeccion> listaProy = new List<Proyeccion>();

        private List<int> lista = new List<int>();//reserva de butacas seleccionada
        private Cliente cliente = new Cliente();
        private Proyeccion proyeccion = new Proyeccion();
        private Usuario user;

        public Usuario User
        {
            get { return user; }
            set { user = value; }
        }


        public winTicket(Usuario usu)
        {
            InitializeComponent();
            Butaca butaca = new Butaca();
            butaca = trabajarTicket.getButaca();
            txtFilaButaca.Text = butaca.But_Fila;
            txtNumeroButaca.Text = butaca.But_Numero;
            txtFecha.Text = "Fecha: "+DateTime.Now.ToLongDateString() + "Hora: " + DateTime.Now.ToLongTimeString();
            lblApellido.Content = usu.Usu_ApellidoNombre;
            this.user = usu;
        }

        private void cargarComboProyeccion()
        {
            cmbProyeccion.DisplayMemberPath = "Proyeccion";
            cmbProyeccion.SelectedValuePath = "proy_Id";
            cmbProyeccion.ItemsSource = TrabajarProyeccion.traerProyecciones().DefaultView;
            cmbProyeccion.SelectedValue = 1;
        }

        public void imprimir(Ticket ticket) {
            //cambiar la asignacion del nro de ticket

            cliente.Cli_DNI = ticket.Cli_Dni;
            cliente.Cli_Apellido = txtApellido.Text;
            cliente.Cli_Nombre = txtNombre.Text;
            proyeccion.Proy_Codigo = ticket.Proy_codigo;
            proyeccion.Proy_Fecha = txtFechProy.Text;
            proyeccion.Proy_Hora = "";
            proyeccion.Sala_Numero = int.Parse(txtSala.Text);
            but.But_Fila = txtFilaButaca.Text;
            but.But_Numero = txtNumeroButaca.Text;
            but.But_NroSala = proyeccion.Sala_Numero.ToString();
            WinVentaTickets win = new WinVentaTickets(but, cliente, proyeccion, ticket, this.user);
            win.Show();
            clear();
        }
        
        private void btnCliente_Click(object sender, RoutedEventArgs e)
        {
            winListaClientes win = new winListaClientes();
            win.ShowDialog();
        }

        private void txtCod_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtCod.Text != "")
            {
                Proyeccion proy = TrabajarProyeccion.obtenerProy(int.Parse(txtCod.Text));
                if (proy != null)
                {
                    Pelicula peli = TrabajarPelicula.buscarPeliculaByCodigo(proy.Peli_Codigo);
                    txtSala.Text = proy.Sala_Numero.ToString();
                    txtFechProy.Text = proy.Proy_Fecha + " - " + proy.Proy_Hora;
                    txtNombrePelicula.Text = peli.Peli_Titulo;
                }
                else {
                    txtSala.Text = "";
                    txtFechProy.Text = "";
                    txtNombrePelicula.Text = "";
                }
                    regenerarButtaca();
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            //listaProyecion win = new listaProyecion();
            //win.Show();

            winListaProyecc listaProyecc = new winListaProyecc();
            listaProyecc.Show();
        }

        
        
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            Ticket ticket = new Ticket();
            if (txtApellido.Text == "" || txtNumeroButaca.Text == "" || txtFilaButaca.Text == "" || txtSala.Text == "")
            {
                MessageBox.Show("Debe Completar todos los campos");
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("¿Venta concretada?", "Agregar Ticket", MessageBoxButton.YesNo);
                if(result == MessageBoxResult.Yes){
                    int cantidadTicketMore = TrabajarTicket.getTicketsCount() + 1; // 100
                    int mes = DateTime.Now.Month;
                    int dia = DateTime.Now.Day;
                    string fecha = mes.ToString() + dia.ToString();
                    string codigoTicket = txtCod.Text + fecha + cantidadTicketMore.ToString();
                    ticket.Tick_Nro = int.Parse(codigoTicket);

                    ticket.But_fila = txtFilaButaca.Text;
                    ticket.But_numero = txtNumeroButaca.Text;
                    ticket.Cli_Dni = int.Parse(txtDni.Text);
                    ticket.Proy_codigo = int.Parse(txtCod.Text);
                    ticket.Tick_FechaVenta = DateTime.Now;
                    TrabajarTicket.AltaTicket(ticket);
                    MessageBox.Show("Ticket guardado con exito");
                    MessageBoxResult result1 = MessageBox.Show("¿Desea imprimirlo?", "Imprimir Ticket", MessageBoxButton.YesNo);
                    if (result1 == MessageBoxResult.Yes)
                    {
                        imprimir(ticket);
                    }
                    regenerarButtaca();
                    clear();
                }
            }
            
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            clear();
        }

        private void ValidarNumButaca(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^1-4]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ValidarFilaButaca(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^A,B,C,D,E,a,b,c,d,e]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void ValidarTextBoxNumber(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        //--------------------------------------------------
        
        void regenerarButtaca() { 
        
            Sala sala = new Sala();
            butacas.Children.Clear();
            if (txtCod.Text != "") {
                try{
                    generarButacas(trabajarSala.traerSalaTipo(txtSala.Text));
                }
                catch {
                    butacas.Children.Clear();
                }
            }
            else
            {
            butacas.Children.Clear();
            }
            
    }
        //--------------------------------------
        private void generarButacas(int capacidad)
        {
            int i, j;
            for (i = 1; i <= capacidad; i++)
            {
                Button b = new Button();
                b.Content = nombreButaca(i);
                if (isButacaDisponible(nombreButaca(i)))
                {
                    b.Background = Brushes.LightGray;
                } else {
                    b.Background = Brushes.Red;
                }
                b.Width = 50;
                for (j = 0; j <= lista.Count - 1; j++)//reserva de butacas seleccionada
                {
                    if (i == lista[j])
                    {
                        b.Background = Brushes.Red;
                    }
                }
                b.Click += (s, e) =>
                {
                    Button button = s as Button;
                    String posicionButaca = button.Content.ToString();

                    if (button.Background == Brushes.Red) {
                        MessageBox.Show("El asiento esta reservado");
                    }
                    else
                    if (button.Background == Brushes.Green) {
                        cargarButaca(" ");
                        button.Background = Brushes.Gray;
                    } else {
                        if (txtFilaButaca.Text != "" && txtNumeroButaca.Text != "") {
                            // MessageBox.Show("Solo se permite una butaca");
                        } else {
                            cargarButaca(posicionButaca);
                            button.Background = Brushes.Green;
                        }
                    }

                };
                if (i <= capacidad)
                {
                    butacas.Children.Add(b);
                }
            }
        }
        void cargarButaca (String posicionButaca) {

            string[] valores = posicionButaca.Split(gSEPARADOR_BUTACA);

            String filaButaca = valores[0];
            String numeroButaca = valores[1];
            txtFilaButaca.Text = filaButaca;
            txtNumeroButaca.Text = numeroButaca;

             Cliente clienteAux = new Cliente();
        }

        String nombreButaca (int posicionButaca){
            int FILA = 5;
            int posicionColumna = (posicionButaca) % FILA;
            int posicionFila = (posicionButaca - 1) / FILA;

            String nombreColumna = "";
            String nombreFila = (posicionFila + 1).ToString();
            switch (posicionColumna) {
                case 1:
                    nombreColumna = "A";
                break;
                case 2:
                    nombreColumna = "B";
                break;
                case 3:
                    nombreColumna = "C";
                break;
                case 4:
                    nombreColumna = "D";
                break;
                default:
                    nombreColumna = "E";
                break;
            }
            return nombreColumna + gSEPARADOR_BUTACA + nombreFila;
        }
        bool isButacaDisponible(String nombreButaca){

            string[] valores = nombreButaca.Split(gSEPARADOR_BUTACA);

            String filaButaca = valores[0];
            String numeroButaca = valores[1];
            int nroProyeccion = int.Parse(txtCod.Text);
            //modificando fila y columna por cod
            int codigoButaca = trabajarTicket.traerCodigoButaca(filaButaca, numeroButaca);
            if (codigoButaca == 0) {
                return true;
            }
            Ticket ticket = trabajarTicket.getTicketCliente(nroProyeccion, codigoButaca);

            if (ticket == null) {
                return true;
            }
            // clienteAux = TrabajarCliente.obtenerCliente(int.Parse(dni));
            return false;
        }
        //-------------------------------------------
        //private void cargarCombo()
        //{
        //    cmbSala.DisplayMemberPath = "DATOS";
        //    cmbSala.SelectedValuePath = "sala_numero";
        //    cmbSala.ItemsSource = trabajarSala.listarSala().DefaultView;
        //    cmbSala.SelectedValue = 1;
        //}

        public void clear()
        {
            txtApellido.Text = "";
            txtCod.Text = "";
            txtDni.Text = "";
            txtFecha.Text = "Fecha: " + DateTime.Now.ToLongDateString() + "Hora: " + DateTime.Now.ToLongTimeString();
            txtFechProy.Text = "";
            txtNombre.Text = "";
            txtSala.Text = "";
            /* butaca seleccionada */
            txtNumeroButaca.Text = "";
            txtFilaButaca.Text = "";
        }

        private void txtDni_TextChanged(object sender, TextChangedEventArgs e)
        {
            Cliente cliente = new Cliente();
            if (txtDni.Text != "" && txtDni.Text.Length < 9)
            {
                cliente = TrabajarCliente.obtenerCliente(int.Parse(txtDni.Text));
                if (cliente != null)
                {
                    txtApellido.Text = cliente.Cli_Apellido;
                    txtNombre.Text = cliente.Cli_Nombre;
                }
            }
            else
            {
                txtApellido.Text = "";
                txtNombre.Text = "";
            }
        }

        private void cmbProyeccion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
